package com.example.toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.app_bar_menu,menu);
        return super.onCreateOptionsMenu(menu);

//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.app_bar_menu,menu);
//        return true;



    }





    //@Override
    //public boolean onOptionsItemSelected(@NonNull MenuItem item) {

//        switch (item.getItemId()){
//
//            case R.id.action_settings:
//                Toast.makeText(this,"Settings Option Selected",Toast.LENGTH_SHORT).show();
//                return(true);
//
//            case R.id.profile_settings:
//                Toast.makeText(this,"Profile Settings Option Selected",Toast.LENGTH_SHORT).show();
//                return(true);
//
//            case R.id.payment_settings:
//                Toast.makeText(this,"Payment Settings Option Selected",Toast.LENGTH_SHORT).show();
//                return(true);
//        }
//
//    }


//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//
//        switch (item.getItemId()){
//
//            case R.id.action_settings:
//                Toast.makeText(this,"Settings Option Selected",Toast.LENGTH_SHORT).show();
//                return(true);
//
//            case R.id.profile_settings:
//                Toast.makeText(this,"Profile Settings Option Selected",Toast.LENGTH_SHORT).show();
//                return(true);
//
//            case R.id.payment_settings:
//                Toast.makeText(this,"Payment Settings Option Selected",Toast.LENGTH_SHORT).show();
//                return(true);
//        }
//        return super.onOptionsItemSelected(item);
//    }

//    public void permissionSettings(MenuItem item) {
//
//        Intent intent = new Intent(this,SettingActivity.class);
//        startActivity(intent);
//        //Toast.makeText(this,"Permission Settings Option Selected",Toast.LENGTH_SHORT).show();
//
//
//    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.permission_settings){
            startActivity(new Intent(this,SettingActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
